"""PokerAnalyser URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from PokerAnalyser1 import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('register/', views.register, name = 'register'),
    path('notes/', views.notes, name = 'notes'),
    path('notes/<int:note_id>/',views.note_detail, name = 'note-detail'),
    path('notes/<int:note_id>/delete/',views.note_delete, name = 'note-delete'),
    path('notes/add/',views.note_add, name = 'note-add'),
    path('hands/',views.hands, name = 'hands'),
    path('hands/<int:hand_id>/',views.hand_detail, name = 'hand-detail'),
    path('hands/<int:hand_id>/delete/',views.hand_delete, name = 'hand-delete'),
    path('hands/add/',views.hand_add, name = 'hand-add'),
    path('hands/<int:hand_id>/players/',views.hand_players, name = 'hand-players'),
    path('hands/<int:hand_id>/actions/',views.actions, name = 'actions'),
    path('tables/',views.tables, name = 'tables'),
]
