from django.apps import AppConfig


class Pokeranalyser1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'PokerAnalyser1'
