from msilib.schema import tables
from wsgiref.handlers import format_date_time
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from .models import User, Note, Player, Hand, Table, Hand_player, Action
from datetime import  timezone, datetime, timedelta
import pytz
from dateutil import parser

def index(request):
    return render(request, "PokerAnalyser1/index.html")

def logout(request):
    del request.session["User"]
    return HttpResponseRedirect("/login")

def login(request):
    output_msg = None
    if request.method == "POST":
    
        form_username = request.POST.get("username", False)
        form_password = request.POST.get("password", False)

        if form_username and form_password:
            try:
                user = User.objects.get(username=form_username)
            except User.DoesNotExist:
                user = None

            if user is not None:

                if user.password == form_password:
                    request.session["User"] = user.username
                    return HttpResponseRedirect("/")
                else:
                    output_msg = "Heslo není správné."
            else:
                output_msg = "Daný uživatel neexistuje."
        else:
            output_msg = "Vyplňte prosím obě pole."

    return render(request, "PokerAnalyser1/login.html", {"output_msg": output_msg})

def register(request):

    output_msg = None
    if request.method == "POST":
    
        form_username = request.POST.get("username", False)
        form_password = request.POST.get("password", False)
        form_password_check = request.POST.get("password_check", False)

        if form_username and form_password and form_password_check:
            try:
                user = User.objects.get(username=form_username)
            except User.DoesNotExist:
                user = None

            if user is None:
                if len(form_username) <= User._meta.get_field("username").max_length:

                    if len(form_password) <= User._meta.get_field("password").max_length:

                        if form_password == form_password_check:

                            user = User(username = form_username, password = form_password)
                            user.save()
                            output_msg = "Registrace proběhla úspěšně"
                        else:
                            output_msg = "Heslo bylo zadáno dvakrát jinak."
                    else:
                         output_msg = "Heslo je příliš dlouhé."   
                else:
                    output_msg = "Uživatelské jméno je příliš dlouhé."
            else:
                output_msg = "Uživatelské jméno již není dostupné."
        else:
            output_msg = "Vyplňte prosím obě pole."

    return render(request, "PokerAnalyser1/register.html", {"output_msg": output_msg})

def notes(request):

    notes = Note.objects.filter(user = User.objects.get(username = request.session["User"]))
    
    return render( request, "PokerAnalyser1/notes.html", {"notes": notes})

def note_detail(request, note_id):

    note = get_object_or_404(Note, pk=note_id)
    output_msg = None
    if request.method == "POST":
        form_player = request.POST.get("player", False)
        form_text = request.POST.get("text", False)

        if form_player and form_text:
        
            player = Player.objects.get(nickname=form_player)

            if len(form_text) <= Note._meta.get_field("text").max_length:

                note.text = form_text
                note.player = player
                note.save()
                output_msg = "Poznámka byla upravena."
                    
            else:
                output_msg = "Text poznámky je příliš dlouhý."
        else:
            output_msg = "Vyplňte prosím obě pole."

    players = Player.objects.filter(user = User.objects.get(username = request.session["User"]) )
    return render( request, "PokerAnalyser1/note_detail.html", {"note": note, "players": players, "output_msg":output_msg})

def note_delete(request, note_id):
    note = get_object_or_404(Note, pk=note_id)
    note.delete()

    return HttpResponseRedirect("/notes")

def note_add(request):

    output_msg = None
    if request.method == "POST":
        form_player = request.POST.get("player", False)
        form_text = request.POST.get("text", False)

        if form_player and form_text:
        
            player = Player.objects.get(nickname=form_player, user = User.objects.get(username = request.session["User"]))

            if len(form_text) <= Note._meta.get_field("text").max_length:

                note = Note(text=form_text, player=player, user = User.objects.get(username = request.session["User"]))
                note.save()
                return HttpResponseRedirect("/notes")
                    
            else:
                output_msg = "Text poznámky je příliš dlouhý."
        else:
            output_msg = "Vyplňte prosím obě pole."

    players = Player.objects.filter(user = User.objects.get(username = request.session["User"]) )
    return render( request, "PokerAnalyser1/note_add.html", {"players": players, "output_msg":output_msg})

def hands(request):

    hands = Hand.objects.filter(user = User.objects.get(username = request.session["User"]))
    
    return render( request, "PokerAnalyser1/hands.html", {"hands": hands})

def hand_detail(request, hand_id):

    hand = get_object_or_404(Hand, pk=hand_id)
    output_msg = None
    if request.method == "POST":
        form_tablename = request.POST.get("table", False)
        form_num = request.POST.get("num", False)
        form_time = request.POST.get("time", False)
        form_rake = request.POST.get("rake", False)
        form_pot = request.POST.get("pot", False)
        form_card_1 =  request.POST.get("card_1", False)
        form_card_2 =  request.POST.get("card_2", False)
        form_card_3 =  request.POST.get("card_3", False)
        form_card_4 =  request.POST.get("card_4", False)
        form_card_5 =  request.POST.get("card_5", False)

        if form_tablename and form_num and form_time and form_rake and form_pot:
        
            new_num_hand = Hand.objects.filter(num=form_num).filter(user = User.objects.get(username = request.session["User"])).first()


            if new_num_hand == hand or new_num_hand is None:
                table = Table.objects.get(name = form_tablename, user = User.objects.get(username = request.session["User"]))
                hand.table=table
                hand.num = form_num
                hand.time = parser.parse(form_time)
                hand.rake = form_rake
                hand.total_pot = form_pot
                if len(form_card_1) == 2:
                    hand.card_1 = form_card_1
                if len(form_card_2)  == 2:
                  hand.card_2 = form_card_2
                if len(form_card_3)  == 2:
                    hand.card_3 = form_card_3
                if len(form_card_4)  == 2:
                    hand.card_4 = form_card_4
                if len(form_card_5)  == 2:
                    hand.card_5 = form_card_5

                hand.save()
                output_msg = "Handa byla upravena."
            else:
                output_msg = "Handa s daným číslem už existuje."
        else:
            output_msg = "Nepovinná pole jsou pouze karty."

    tz = pytz.timezone('Europe/Prague')
    iso_date = hand.time.astimezone(tz).strftime('%Y-%m-%dT%H:%M')
    tables = Table.objects.filter(user = User.objects.get(username = request.session["User"]) )
    return render( request, "PokerAnalyser1/hand_detail.html", {"hand": hand, "output_msg":output_msg, "tables":tables, "iso_date":iso_date})

def hand_delete(request, hand_id):
    hand = get_object_or_404(Hand, pk=hand_id)
    hand.delete()

    return HttpResponseRedirect("/hands")

def hand_add(request):

    output_msg = None
    if request.method == "POST":
        form_tablename = request.POST.get("table", False)
        form_num = request.POST.get("num", False)
        form_time = request.POST.get("time", False)
        form_rake = request.POST.get("rake", False)
        form_pot = request.POST.get("pot", False)
        form_card_1 =  request.POST.get("card_1", False)
        form_card_2 =  request.POST.get("card_2", False)
        form_card_3 =  request.POST.get("card_3", False)
        form_card_4 =  request.POST.get("card_4", False)
        form_card_5 =  request.POST.get("card_5", False)


        if form_tablename and form_num and form_time and form_rake and form_pot:
        
            new_num_hand = Hand.objects.filter(num=form_num).filter(user = User.objects.get(username = request.session["User"])).first()


            if  new_num_hand is None:
                table = Table.objects.get(name = form_tablename, user = User.objects.get(username = request.session["User"]))

                hand = Hand(table=table, num=form_num, time=parser.parse(form_time), rake=form_rake, total_pot=form_pot, user = User.objects.get(username = request.session["User"]))
                if len(form_card_1) == 2:
                    hand.card_1 = form_card_1
                if len(form_card_2)  == 2:
                  hand.card_2 = form_card_2
                if len(form_card_3)  == 2:
                    hand.card_3 = form_card_3
                if len(form_card_4)  == 2:
                    hand.card_4 = form_card_4
                if len(form_card_5)  == 2:
                    hand.card_5 = form_card_5

                hand.save()
                return HttpResponseRedirect("/hands")
            else:
                output_msg = "Handa s daným číslem už existuje."
        else:
            output_msg = "Nepovinná pole jsou pouze karty."

    tables = Table.objects.filter(user = User.objects.get(username = request.session["User"]) )
    return render( request, "PokerAnalyser1/hand_add.html", {"output_msg":output_msg, "tables":tables})

def hand_players(request, hand_id):
    hand = get_object_or_404(Hand, pk=hand_id)
    hand_players = Hand_player.objects.filter(user = User.objects.get(username = request.session["User"])).filter(hand = hand)
    
    return render( request, "PokerAnalyser1/hand_players.html", {"hand_players":hand_players})

def actions(request, hand_id):
    hand = get_object_or_404(Hand, pk=hand_id)
    actions = Action.objects.filter(user = User.objects.get(username = request.session["User"])).filter(hand = hand).order_by('sequence')
    
    return render( request, "PokerAnalyser1/actions.html", {"actions":actions})

def tables(request):
    tables = Table.objects.filter(user = User.objects.get(username = request.session["User"]))

    return render(request, "PokerAnalyser1/tables.html", {"tables":tables})