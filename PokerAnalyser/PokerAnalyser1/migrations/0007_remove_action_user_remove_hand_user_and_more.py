# Generated by Django 4.0.4 on 2022-05-15 09:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('PokerAnalyser1', '0006_action_user_hand_user_hand_player_user_note_user_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='action',
            name='user',
        ),
        migrations.RemoveField(
            model_name='hand',
            name='user',
        ),
        migrations.RemoveField(
            model_name='hand_player',
            name='user',
        ),
        migrations.RemoveField(
            model_name='note',
            name='user',
        ),
        migrations.RemoveField(
            model_name='player',
            name='user',
        ),
        migrations.RemoveField(
            model_name='table',
            name='user',
        ),
    ]
