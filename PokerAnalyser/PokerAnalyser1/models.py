from django.db import models
from django.core.validators import MinLengthValidator


class User(models.Model):
    username = models.CharField(max_length=50,unique=True)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.username

class Table(models.Model):

    user = models.ForeignKey(User,on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    currency = models.CharField(max_length=3)
    sb_size = models.DecimalField(max_digits=19, decimal_places=2)
    bb_size = models.DecimalField(max_digits=19, decimal_places=2)
    ante_size = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
        return self.name


class Hand(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    num = models.BigIntegerField()
    time = models.DateTimeField("time played")
    rake = models.DecimalField(max_digits=19, decimal_places=2)
    total_pot = models.DecimalField(max_digits=19, decimal_places=2)
    card_1 = models.CharField(validators=[MinLengthValidator(2)], max_length=2,default=None, blank=True, null=True)
    card_2 = models.CharField(validators=[MinLengthValidator(2)], max_length=2, default=None, blank=True, null=True)
    card_3 = models.CharField(validators=[MinLengthValidator(2)], max_length=2, default=None, blank=True, null=True)
    card_4 = models.CharField(validators=[MinLengthValidator(2)], max_length=2, default=None, blank=True, null=True)
    card_5 = models.CharField(validators=[MinLengthValidator(2)], max_length=2, default=None, blank=True, null=True)
    
    def __str__(self):
        return str(self.num)



class Player(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    nickname = models.CharField(max_length=50)

    def __str__(self):
        return self.nickname



class Note(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    created_at = models.DateTimeField("time created", auto_now_add=True)

    def __str__(self):
        return f"{self.id}, {self.player.nickname}"


class Hand_player(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    hand = models.ForeignKey(Hand, on_delete=models.CASCADE)
    player = models.ForeignKey(
        Player, on_delete=models.CASCADE)
    seat = models.SmallIntegerField()
    position = models.SmallIntegerField()
    initial_stack = models.DecimalField(max_digits=19, decimal_places=2)
    collected = models.DecimalField(max_digits=19, decimal_places=2)
    invested = models.DecimalField(max_digits=19, decimal_places=2)
    returned_or_blind = models.DecimalField(max_digits=19, decimal_places=2)
    hole_card_1 = models.CharField(
        validators=[MinLengthValidator(2)], max_length=2, default=None, blank=True, null=True)
    hole_card_2 = models.CharField(
        validators=[MinLengthValidator(2)], max_length=2, default=None, blank=True, null=True)
    won_hand = models.BooleanField()
    went_to_showdown = models.BooleanField()

    def __str__(self):
        return f"{self.hand.num}, {self.player.nickname}"


class Action(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    hand = models.ForeignKey(Hand, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    sequence = models.SmallIntegerField()
    type = models.CharField(validators=[MinLengthValidator(1)], max_length=1)
    street = models.CharField(validators=[MinLengthValidator(1)], max_length=1)
    invested = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
         return f"{self.hand.num}, {self.player.nickname}, {self.sequence}"

