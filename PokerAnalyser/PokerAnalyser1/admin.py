from django.contrib import admin
from .models import Table, Hand, Player, Note, Hand_player, Action,User

admin.site.register(Table)
admin.site.register(Hand)
admin.site.register(Player)
admin.site.register(Note)
admin.site.register(Hand_player)
admin.site.register(Action)
admin.site.register(User)