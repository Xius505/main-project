from django.http import HttpResponseRedirect

class loginMiddleware():
    def __init__(self, get_response):
        self.get_response = get_response
    
    def __call__(self, request):
        if not request.path_info.startswith("/login") and not request.path_info.startswith("/register") and not request.path_info.startswith("/admin"):
            if not request.session.get("User", False):
                return HttpResponseRedirect("/login")
        
        response = self.get_response(request)

        return response