# Poker Analyser

Project is a Django web application which serves as an online poker tracker database.
Users can create accounts and then manage their hand history and notes on other players with persistent data storing.

## Installation

Using pip install -r requirements.txt (from PokerAnalyser folder), recommeded using virtual environment.

## Dependencies

Django framework, python packages in requirements.txt (from PokerAnalyser folder).
